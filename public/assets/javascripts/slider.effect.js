var _SlideshowTransitionC = {};
var _SlideshowTransitionCodes = {};
var _SlideshowTransitions = [];

//----------- Expand Effects --------------
{

    _SlideshowTransitionC["Expand Stairs"] = { $Duration: 1000, $Delay: 30, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 2050, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Stairs"] = "{$Duration:1000,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:$JssorEasing$.$EaseInQuad}";

    // _SlideshowTransitionC["Expand Straight"] = { $Duration: 1000, $Cols: 3, $Rows: 2, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationStraight, $Easing: $JssorEasing$.$EaseInBounce };
    // _SlideshowTransitionCodes["Expand Straight"] = "{$Duration:1000,$Cols:3,$Rows:2,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:$JssorEasing$.$EaseInBounce}";

    _SlideshowTransitionC["Expand Swirl"] = { $Duration: 500, $Delay: 30, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationSwirl, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Swirl"] = "{$Duration:500,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationSwirl,$Easing:$JssorEasing$.$EaseInQuad}";

    _SlideshowTransitionC["Expand Square"] = { $Duration: 800, $Delay: 300, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationSquare, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Square"] = "{$Duration:800,$Delay:300,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationSquare,$Easing:$JssorEasing$.$EaseInQuad}";

    _SlideshowTransitionC["Expand Rectangle Cross"] = { $Duration: 800, $Delay: 300, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationRectangleCross, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Rectangle Cross"] = "{$Duration:800,$Delay:300,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationRectangleCross,$Easing:$JssorEasing$.$EaseInQuad}";

    _SlideshowTransitionC["Expand Rectangle"] = { $Duration: 800, $Delay: 300, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationRectangle, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Rectangle"] = "{$Duration:800,$Delay:300,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationRectangle,$Easing:$JssorEasing$.$EaseInQuad}";

    _SlideshowTransitionC["Expand Cross"] = { $Duration: 800, $Delay: 300, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationCross, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Cross"] = "{$Duration:800,$Delay:300,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationCross,$Easing:$JssorEasing$.$EaseInQuad}";

    _SlideshowTransitionC["Expand ZigZag"] = { $Duration: 500, $Delay: 30, $Cols: 8, $Rows: 4, $Clip: 15, $Formation: $JssorSlideshowFormations$.$FormationZigZag, $Assembly: 260, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand ZigZag"] = "{$Duration:500,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationZigZag,$Assembly:260,$Easing:$JssorEasing$.$EaseInQuad}";

    _SlideshowTransitionC["Expand Random"] = { $Duration: 1000, $Delay: 80, $Cols: 8, $Rows: 4, $Clip: 15, $Easing: $JssorEasing$.$EaseInQuad };
    _SlideshowTransitionCodes["Expand Random"] = "{$Duration:1000,$Delay:80,$Cols:8,$Rows:4,$Clip:15,$Easing:$JssorEasing$.$EaseInQuad}";
}

$Jssor$.$Each(_SlideshowTransitionC, function (slideshowTransition, name) {
    _SlideshowTransitions.push(slideshowTransition);
});

jssor_slider1_starter = function (containerId) {
    var jssor_slider1 = new $JssorSlider$(containerId, {
        $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
        $Idle: 3500,                            			//[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
        $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)
        $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
        $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
            $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
            $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
            $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
            $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
            $Scale: false                                   //Scales bullets navigator or not while slider scale
        },

        $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
            $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
            $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
            $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
            $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
            $Rows: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
            $SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
            $SpacingY: 4,                                   //[Optional] Vertical space between each item in pixel, default value is 0
            $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
            $Scale: false                                   //Scales bullets navigator or not while slider scale
        },
        $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
            $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
            $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
            $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
            $ShowLink: true                                 //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
        }
    });

    PlaySlideshowTransition = function (event) {
        $Jssor$.$StopEvent(event);
        $Jssor$.$CancelEvent(event);

        try {
            var eventSrcElement = $Jssor$.$EvtSrc(event);
            var transitionName = $Jssor$.$InnerText(eventSrcElement);
            jssor_slider1.$Next();

            jssor_slider1.$SetSlideshowTransitions([_SlideshowTransitionC[transitionName]]);

            var effectStr = _SlideshowTransitionCodes[transitionName];

            if (transitionNameTextBox) {
                transitionNameTextBox.value = transitionName;
            }
            if (transitionCodeTextBox) {
                transitionCodeTextBox.value = effectStr;
            }
        }
        catch (e) { }
    }

    TransitionTextBoxClickEventHandler = function (event) {
        transitionCodeTextBox.select();

        $Jssor$.$CancelEvent(event);
        $Jssor$.$StopEvent(event);
    }

    var transitionCodeTextBox = $Jssor$.$GetElement("stTransition");
    var transitionNameTextBox = $Jssor$.$GetElement("stTransitionName");
    $Jssor$.$AddEvent(transitionCodeTextBox, "click", TransitionTextBoxClickEventHandler);

    //responsive code begin
    //you can remove responsive code if you don't want the slider scales while window resizing
    function ScaleSlider() {
        var bodyWidth = document.body.clientWidth;
        if (bodyWidth)
            jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 600));
        else
            $Jssor$.$Delay(ScaleSlider, 30);
    }

    ScaleSlider();
    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    //responsive code end
};