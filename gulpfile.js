var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minifycss = require('gulp-minify-css');
var minifyhtml = require('gulp-minify-html');
var browserSync = require('browser-sync').create();

gulp.task('uglify', function(){
   return gulp.src('public/js/common.js')
                .pipe(uglify())
                .pipe(gulp.dest('dist/js'))
});

gulp.task('default', ['uglify']);