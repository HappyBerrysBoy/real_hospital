var express = require('express');
var requesthandler = require('../lib/requesthandler.js');
var router = express.Router();
var async = require('async');
var passwordHash = require('password-hash');
var multer = require('multer');
var string = require('string');
var upload = multer({ 
  dest: string(__dirname).replace('routes', '') +'public/images/popup/',
  limits: {fileSize: 10000000, files:1}
});

router.route('/signin')
    .get(function(req, res, next) {
        res.render('users/signin', { ss:req.session});
    })
    .post(function(req, res, next) {
      var param = new Array();
      param.push(req.body);
       
	    requesthandler('HOSPITAL-CUSTOMER-EXESIGNIN', param, function(err, val){
   			if(err){ 
   			    res.send({'error' : err, ss:req.session });
   			} else if(val.length == 0) {
      			res.send( {'error' : '존재하지 않는 ID입니다.', ss:req.session});
      	} else {
      		    if(passwordHash.verify(req.body.password, val[0].password)) {
      		        var session = req.session;
      		        session.name = val[0].name;
                      session.admin = val[0].admin;
      		        res.send({link : '/', ss:req.session});
          		} else {
                      //session.userid = val[0].id;
          			res.send({error : '패스워드 오류입니다', ss:req.session});
          		}
      	}
  		});
	});

router.route('/signout')
    .get(function(req, res, next) {
        req.session.name = '';
        req.session.admin = '';
        res.redirect('/');
  /*      req.session.destroy(function(err){
            if(err){
                console.log(err);
            }
            else
            {
                res.redirect('/');
            }
        });*/
    });

router.route('/admin')
    .get(function(req, res, next) {
        if(req.session.name == '' || req.session.admin != 'Y'){
            res.redirect('/');
        }

        var tab = 1;
        if(req.query.tab != undefined){
            tab = req.query.tab;
        }

        var page = 1;
        if(req != null && typeof req.query.page != "undefined") page = req.query.page;
        var appPage = 1;
        if(req != null && typeof req.query.appPage != "undefined") appPage = req.query.appPage;
        var noticePage = 1;
        if(req != null && typeof req.query.noticePage != "undefined") noticePage = req.query.noticePage;
        var userPage = 1;
        if(req != null && typeof req.query.userPage != "undefined") userPage = req.query.userPage;

        var cnt = 5;
          async.parallel([
            function getlist(callback){
              var param = new Array();
              param.push({'start_idx' : (page - 1) * cnt  + 1, 'end_idx' : page * cnt});
                requesthandler('HOSPITAL-CONTACT-LSTCOUNSEL', param, function(err, val){
                  if(err) console.log(err);
                  else callback(null, val);
                }
              );
            },
            function getlistcnt(callback){
              requesthandler('HOSPITAL-CONTACT-GETCOUNSELCNT', null, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            },
            function getAppointmentlist(callback){
                var param = new Array();
                param.push({'start_idx' : (appPage - 1) * cnt  + 1, 'end_idx' : appPage * cnt});
                  requesthandler('HOSPITAL_CONATCT_LSTAPPOINTMENT', param, function(err, val){
                    if(err) console.log(err);
                    else callback(null, val);
                  }
                );
            },
            function getAppointmentlistcnt(callback){
              requesthandler('HOSPITAL_CONTACT_GETAPPOINTMENTCNT', null, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            },
            function getNoticeist(callback){
                var param = new Array();
                param.push({'start_idx' : (noticePage - 1) * cnt  + 1, 'end_idx' : noticePage * cnt});
                  requesthandler('HOSPITAL_ADMIN_LSTNOTICE', param, function(err, val){
                    if(err) console.log(err);
                    else callback(null, val);
                  }
                );
            },
            function getNoticecnt(callback){
              requesthandler('HOSPITAL_ADMIN_GETNOTICECNT', null, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            },
            function getUserlistcnt(callback){
                var param = new Array();
                param.push({'start_idx' : (userPage - 1) * cnt  + 1, 'end_idx' : userPage * cnt});
              requesthandler('HOSPITAL-ADMIN-LSTUSERS', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            },
            function getUsercnt(callback){
              requesthandler('HOSPITAL-ADMIN-GETUSERCNT', null, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            },
            function getPopuplist(callback){
                var param = new Array();
                param.push({'start_idx' : (userPage - 1) * cnt  + 1, 'end_idx' : userPage * cnt, seq:null, use_tag:null});
                requesthandler('HOSPITAL-ADMIN-LSTPOPUP', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            },
            function getPopupcnt(callback){
              requesthandler('HOSPITAL-ADMIN-GETPOPUPCNT', null, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              });
            }],
            function(err, results){
              if(err) return console.log(err);
              res.render('users/admin', { 
                                            list:results[0],
                                            cnt:results[1],
                                            appList:results[2],
                                            appCnt:results[3],
                                            noticeList:results[4],
                                            noticeCnt:results[5],
                                            userList:results[6],
                                            userCnt:results[7],
                                            popupList:results[8],
                                            popupCnt:results[9],
                                            ss:req.session,
                                            tab:tab
                                        });
            }
        );  
    });

router.route('/signup')
    .get(function(req, res, next) {
        res.render('users/signup', { title: 'Express', ss:req.session});
    })
    .post(function(req, res, next) {
        if(req.body)
    	{
    		var param = new Array();
            req.body.email += '@' + req.body.emailExt;
    		param.push(req.body);
    		var hashedPassword = '';
    		async.series([
    			function(callback){
    				requesthandler('HOSPITAL-CUSTOMER-GETINFO', param, function(err, val){
    					if(err) callback(err);
    					else if(val.length > 0) callback(req.body.email + '는 이미 존재합니다.'); 
    					else callback(null, 1);
    				});
    			},
    			function(callback){
    				var password = req.body.password;
    				var confirm = req.body.confirm;
    				if(password != confirm) callback('입력하신 패스워드가 잘못되었습니다.');
    				else callback(null, 2);
    			},
    			function(callback){
    				hashedPassword = passwordHash.generate(req.body.password);
    				if(hashedPassword == '') callback('패스워드가 암호화되지 않았습니다.');
    				else callback(null, 3);
    			},
    			function(callback){
    			    param[0].password = hashedPassword;
    				requesthandler('HOSPITAL-CUSTOMER-CRTINFO', param, function(err, val){
    					if(err) callback(err);
    					else callback(null, 4);
    				});
    			}
    		],
    		function(err, results){
    			if(err) res.send({message : err});
    			else {
    			    res.send({message : '회원가입이 정상처리 되었습니다.', ss:req.session, link : '/users/signin'});
    			}
    		});
    	}
    	else{
    		res.send('input user info');
        }
    });
    
    
router.route('/user/:seq')
   // 3. Update : 관리자 업데이트
    .put(function(req, res, next){
		var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-ADMIN-UPDADMINUSER', param, function(err, val){
            if(err) {
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        }); 
    })
    // 4. Delete : 유저 삭제
    .delete(function(req, res, next){
		var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-ADMIN-DLTUSER', param, function(err, val){
            if(err) {
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        }); 
    });
    
router.route('/popup/new').get(function(req, res, next){
   res.render('category/notice/popup', {data:null, ss:req.session}); 
});
    
router.route('/popup/:seq')
    .get(function(req, res, next) {
    	var param = [{
            use_tag:null,
            seq:req.params.seq,
            start_idx:'1',
            end_idx:'2'
        }];
        
        requesthandler('HOSPITAL-ADMIN-LSTPOPUP', param, function(err, val){
            if(err){
                console.log(err);
            }else{
                if(val.length == 0){
                    res.send('F');
                }else{
                    res.render('category/notice/popup', {data:val, ss:req.session, seq:req.params.seq});
                }
            }
        });
    })
    // .post(function(req, res, next){
	// 	var param = new Array();
    //     param.push(req.body);
        
    //     requesthandler('HOSPITAL-ADMIN-CRTPOPUP', param, function(err, val){
    //         if(err) {
    //             console.log(err);
    //             res.send('F');
    //         } else {
    //             res.send('T');
    //         }
    //     }); 
    // })
    .put(function(req, res, next){
		var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-ADMIN-CRTPOPUP', param, function(err, val){
            if(err) {
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        }); 
    })
    .delete(function(req, res, next){
		var param = new Array();
        param.push({seq:req.params.seq});
        
        requesthandler('HOSPITAL-ADMIN-DLTPOPUP', param, function(err, val){
            if(err) {
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        }); 
    });

router.post('/popup/image_upload', upload.single('file'), function(req, res, next){
        console.log(req.file);
        res.send({url : '/images/popup/' + req.file.filename});
    });
    
module.exports = router;
