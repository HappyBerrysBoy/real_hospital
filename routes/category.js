var express = require('express');
var requesthandler = require('../lib/requesthandler.js');
var router = express.Router();
var async = require('async');
var string = require('string');
var multer = require('multer');
var upload = multer({ 
  dest: string(__dirname).replace('routes', '') +'public/images/counsel/',
  limits: {fileSize: 10000000, files:1}
});
var uploadNotice = multer({ 
  dest: string(__dirname).replace('routes', '') +'public/images/notice/',
  limits: {fileSize: 10000000, files:1}
});

/* GET home page. */
router.get('*', function(req, res, next) {
    async.parallel([
    function getlist(callback){
	    requesthandler('HOSPITAL-CATEGORY-LSTMENU', null, function(err, val){
			if(err) console.log(err);
			else callback(null, val);
       	});
    },
    function getlistcnt(callback){
		requesthandler('HOSPITAL-CATEGORY-LSTSUBMENU', null, function(err, val){
			if(err) console.log(err);
       	    else callback(null, val);
     	});
    }],
    function(err, results){
      if(err) return console.log(err);
      req.session.menu = results[0];
      req.session.sub_menu = results[1];
      next();
    });
});

// 상담/예약 -> 온라인상담
router.route('/contact/counsel_list')
    .get(function(req, res, next){
        var page = 1;
        if(req != null && typeof req.query.page != "undefined") page = req.query.page;
    
        var cnt = 10;
        async.parallel([
          function getlist(callback){
            var param = new Array();
            param.push({'start_idx' : (page - 1) * cnt  + 1, 'end_idx' : page * cnt});
              requesthandler('HOSPITAL-CONTACT-LSTCOUNSEL', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              }
            );
          },
          function getlistcnt(callback){
            requesthandler('HOSPITAL-CONTACT-GETCOUNSELCNT', null, function(err, val){
              if(err) console.log(err);
              else callback(null, val);
            });
          }],
          function(err, results){
            if(err) return console.log(err);
            res.render('category/contact/counsel_list', { list:results[0], cnt:results[1], ss:req.session});
          }
        ); 
    })
    .post(function(req, res, next){
    });

router.route('/contact/counsel_check_authority')
    // 상담내용 조회 권한 체크
    .post(function(req, res, next){
        var arrParam = new Array();
        arrParam.push(req.body);

        requesthandler('HOSPITAL-CONTACT-CHKPASSWORD', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else if(val[0].CNT == 0){
                console.log('Password Error');
                res.send('F');
            } else {
                res.send('T');
            }
        });
    });    
    
router.post('/contact/counsel/image_upload', upload.single('file'), function(req, res, next){
        console.log(req.file);
        res.send({url : '/images/counsel/' + req.file.filename});
    });
    
router.route('/contact/counsel/new')
    // 상담 새글 저장
    .get(function(req, res, next){
        res.render('category/contact/counsel', { counsel_info : '',ss:req.session, type:'I'});
    });

  // 1. Select : 상담내용 편집폼 요청
router.route('/contact/counsel/:seq/edit')  
    .get(function(req, res, next){
        if(req.session.admin == 'Y'){
            var param = new Array();
            param[0] = {seq : req.params.seq};
            
            requesthandler('HOSPITAL-CONTACT-GETCOUNSELFORADMIN', param, function(err, val){
                if(err) {
                    console.log(err);
                    res.send('F');
                } else {
                    res.render('category/contact/counsel', { counsel_info : val[0], ss:req.session, type:'U' });
                }
            });  
        } else {
            res.send('F');
        }
    })
    .post(function(req, res, next){
         var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-CONTACT-GETCOUNSEL', param, function(err, val){
            if(err) {
                console.log(err);
                res.send('F');
            } else {
                res.render('category/contact/counsel', { counsel_info : val[0], ss:req.session, type:'U' });
            }
        });  

    });

router.route('/contact/counsel')
    // 2. Insert : 상담내용 저장
    .post(function(req, res, next){
		var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-CONTACT-CRTCOUNSEL', param, function(err, val){
            if(err) {
                console.log(err);
                res.send({result : 'error', message : err});
            } else {
                res.send( {result : 'success', message : val});
            }
        }); 
       
    });    

router.route('/contact/counsel/:seq')
   // 3. Update : 상담내용 수정
    .put(function(req, res, next){
		var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-CONTACT-UPDCOUNSEL', param, function(err, val){
            if(err) {
                console.log(err);
                res.send({result : 'error', message : err});
            } else {
                res.send( {result : 'success', message : val});
            }
        }); 
    })
    // 4. Delete : 상담내용 삭제
    .delete(function(req, res, next){
		var param = new Array();
        param.push(req.body);
        
        requesthandler('HOSPITAL-CONTACT-DLTCOUNSEL', param, function(err, val){
            if(err) {
                console.log(err);
                res.send({result : 'error', message : err});
            } else {
                res.send( {result : 'success', message : val});
            }
        }); 
    });

router.route('/contact/appointment')
	.get(function(req, res, next){

	})
	.post(function(req, res, next){
		var arrParam = new Array();
		console.log(req.body);
 		arrParam.push(req.body);

 		requesthandler('HOSPITAL_CONTACT_CRTAPPOINTMENT', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        });
	});

router.route('/contact/appointment/:seq')
    .put(function(req, res, next){
		// var arrParam = new Array();
		// console.log(req.params.seq);
        var arrParam = new Array();
        arrParam.push(req.body);
 		// arrParam.push({seq:req.params.seq, status:'Y'});

 		requesthandler('HOSPITAL_CONTACT_UPDAPPOINTMENT', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        });
	})
	.delete(function(req, res, next){
		var arrParam = new Array();
		console.log(req.params.seq);
 		arrParam.push({seq:req.params.seq});

 		requesthandler('HOSPITAL_CONTACT_DLTAPPOINTMENT', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        });
	});

router.route('/notice')
    .get(function(req, res, next) {
        var page = 1;
        if(req != null && typeof req.query.page != "undefined") page = req.query.page;
    
        var cnt = 10;
        async.parallel([
          function getlist(callback){
            var param = new Array();
            param.push({'start_idx' : (page - 1) * cnt  + 1, 'end_idx' : page * cnt});
              requesthandler('HOSPITAL_ADMIN_LSTNOTICE', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
              }
            );
          },
          function getlistcnt(callback){
            requesthandler('HOSPITAL_ADMIN_GETNOTICECNT', null, function(err, val){
              if(err) console.log(err);
              else callback(null, val);
            });
          }],
          function(err, results){
            if(err) return console.log(err);
            res.render('category/notice/notice_list', { list:results[0], cnt:results[1], ss:req.session});
          }
        ); 
    })
    .post(function(req, res, next) {
        var arrParam = new Array();
 		arrParam.push(req.body);

 		requesthandler('HOSPITAL_ADMIN_CRTNOTICE', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        });
    });

router.route('/notice/new')
    .get(function(req, res, next) {
        res.render('category/notice/writenotice', { data:null, ss:req.session, seq:null});
    });

router.route('/notice/:seq')
    .get(function(req, res, next) {
    	var arrParam = new Array();
 		arrParam.push({seq:req.params.seq});

 		requesthandler('HOSPITAL_ADMIN_GETNOTICE', arrParam, function(err, val){
 			console.log(val);
            res.render('category/notice/writenotice', { data:val, ss:req.session, seq:req.params.seq});
        });
    })
    .put(function(req, res, next) {
    	var arrParam = new Array();
		console.log(req.body);
 		arrParam.push(req.body);

 		requesthandler('HOSPITAL_ADMIN_UPDNOTICE', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        });
    })
    .delete(function(req, res, next) {
    	var arrParam = new Array();
		console.log(req.params.seq);
 		arrParam.push({seq:req.params.seq});

 		requesthandler('HOSPITAL_ADMIN_DLTNOTICE', arrParam, function(err, val){
            if(err){
                console.log(err);
                res.send('F');
            } else {
                res.send('T');
            }
        });
    });

 router.post('/notice/image_upload', uploadNotice.single('file'), function(req, res, next){
        console.log(req.file);
        res.send({url : '/images/notice/' + req.file.filename});
    });   
    
router.get('/:sub_menu_id', function(req, res, next) {
    res.render('category/' + req.params.sub_menu_id + '/sub_menu1', { ss:req.session, page : '1' });
});

router.get('/:menu_id/:sub_menu_id', function(req, res, next) {
    res.render('category/' + req.params.menu_id + '/sub_menu1', { ss:req.session, page : req.query.page });
});

module.exports = router;
