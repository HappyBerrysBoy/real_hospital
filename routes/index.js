var express = require('express');
var requesthandler = require('../lib/requesthandler.js');
var router = express.Router();
var async = require('async');

/* GET home page. */
router.get('*', function(req, res, next) {
    async.parallel([
    function getlist(callback){
	    requesthandler('HOSPITAL-CATEGORY-LSTMENU', null, function(err, val){
			if(err) console.log(err);
			else callback(null, val);
       	});
    },
    function getlistcnt(callback){
		requesthandler('HOSPITAL-CATEGORY-LSTSUBMENU', null, function(err, val){
			if(err) console.log(err);
       	    else callback(null, val);
     	});
    }],
    function(err, results){
      if(err) return console.log(err);
      req.session.menu = results[0];
      req.session.sub_menu = results[1];
      next();
    });
});

router.get('/', function(req, res, next) {
    
    var page = 1;
    if(req != null && typeof req.query.page != "undefined") page = req.query.page;

    var cnt = 10;
    async.parallel([
        function getlist(callback){
            var param = [{
                use_tag:'Y',
                seq:null,
                start_idx:'1',
                end_idx:'10'
            }];
            requesthandler('HOSPITAL-ADMIN-LSTPOPUP', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
            });
        },
        function getlistcnt(callback){
            var param = new Array();
            param.push({'start_idx' : (page - 1) * cnt  + 1, 'end_idx' : page * cnt});
            requesthandler('HOSPITAL-CONTACT-LSTCOUNSEL', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
                }
            );
        },
        function getNoticeList(callback){
            var param = new Array();
            param.push({'start_idx' : (page - 1) * cnt  + 1, 'end_idx' : page * cnt});
            requesthandler('HOSPITAL_ADMIN_LSTNOTICE', param, function(err, val){
                if(err) console.log(err);
                else callback(null, val);
                }
            );
        }],
        function(err, results){
            if(err) return console.log(err);
            res.render('index', { popupList:results[0], counselList:results[1], noticeList:results[2], ss:req.session});
        }
    ); 
});

router.get('/popup/getcontent/:seq', function(req, res, next){
    var param = [{
        use_tag:'Y',
        seq:req.params.seq,
        start_idx:'1',
        end_idx:'100'
    }];
    
    requesthandler('HOSPITAL-ADMIN-LSTPOPUP', param, function(err, val){
        if(err){
            console.log(err);
        }else{
            if(val.length == 0){
                res.send('F');
            }else{
                var chkbox = '';
                // chkbox = '"<br><div style="float:right;"><input type="checkbox">오늘하루 열지 않기</input>';
                res.send(val[0].content + chkbox);
            }
        }
    });
});

router.get('/editor', function(req, res, next) {
    res.render('editor', { ss:req.session});
});

router.get('/info', function(req, res, next){
    res.render('category/hospitalinfo/info', { ss:req.session, page:req.query.page});
});

// router.route('/contact')
//   .get(function(req, res, next){
//     var page = 1;
//     if(req != null && typeof req.query.page != "undefined") page = req.query.page;

//     var cnt = 10;
//     async.parallel([
//       function getlist(callback){
//         var param = new Array();
//         param.push({'start_idx' : (page - 1) * cnt  + 1, 'end_idx' : page * cnt});
//           requesthandler('HOSPITAL-CONTACT-LSTCOUNSEL', param, function(err, val){
//             if(err) console.log(err);
//             else callback(null, val);
//           }
//         );
//       },
//       function getlistcnt(callback){
//         requesthandler('HOSPITAL-CONTACT-GETCOUNSELCNT', null, function(err, val){
//           if(err) console.log(err);
//           else callback(null, val);
//         });
//       }],
//       function(err, results){
//         if(err) return console.log(err);
//         res.render('category/contact/contact', { list:results[0], cnt:results[1], ss:req.session});
//       }
//     );  
// });

router.get('/community', function(req, res, next){
    res.render('category/community/community', { ss:req.session});
});

// router.get('/writeout', function(req, res, next){
//     res.render('category/contact/writeout', { ss:req.session, type:'I'});
// });

// router.post('/checkpassword', function(req, res, next){
  
// });

// router.get('/:pages', function(req, res, next) {
//   var page = req.params.pages;
//   res.render(page, { ss:req.session});
// });

router.post('/query/:id', function(req, res, next) {
    var arrParam = new Array();
    arrParam.push(req.body);
    requesthandler(req.params.id, arrParam, function(err, val){
        if(err) console.log(err);
        res.send(val);
    });
});

// router.get('/:menu/:sub_menu_id', function(req, res, next) {
//     res.render('category/' + req.params.sub_menu_id + '/sub_menu1', { name: req.session.name, menu : req.session.menu, sub_menu : req.session.sub_menu, page : '1' });
// });

module.exports = router;
