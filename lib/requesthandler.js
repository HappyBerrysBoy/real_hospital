var mysql = require('mysql');
var async = require('async');
var dbConfig = require('./config/database_new');
var debug = require('debug');
var errorDebug = new debug('request:error');
var executeDebug = new debug('request:execute');
var connectionDebug = new debug('request:connection');

var requesthandler = function(program_id, param, callback)
{
	var pool = mysql.createPool(dbConfig);

	pool.getConnection(function(err, connection){
		if(err){
			connectionDebug(err);
			callback(err);
			return;
		}

		connection.config.queryFormat = function (query, values) {
			if (!values) return query;
			return query.replace(/\:(\w+)/g, function (txt, key) {
				if (values.hasOwnProperty(key)) {
					return this.escape(values[key]);
				}
				
				return txt;
			}.bind(this));
		};

		connection.beginTransaction(function(err){
			if(err) {
				endConnection(err);
				return;
			}

	    	var getSqlCmd = 'select sql_statement, sql_typ from frm_prg where program_id = :program_id';
			var i = 0;
			var paramCnt = 1;

			if(param == null || typeof param == "undefined"){
				param = new Array();
				param.push('');
			}

			paramCnt = param.length;
			var result;

			async.whilst(
				function() {
					return i < paramCnt;
				},
				function(c) {
					connection.query(getSqlCmd, {program_id : program_id}, function(err, sqlInfo, fields) {
						if(err) {
							c(err);
						} else {
							connectionDebug(param[i]);
							connection.query(sqlInfo[0].sql_statement, param[i], function(err, results, fields){
								if(err) {
									c(err);
								} else {
									if(sqlInfo[0].sql_typ == 'I'){
										executeDebug ('insert id : ' + results.insertId);
									} else if(sqlInfo[0].sql_typ == 'U') {
										executeDebug('changed ' + results.changedRows + ' rows');
									} else if(sqlInfo[0].sql_typ == 'D') {
										executeDebug('deleted ' + results.affectedRows + ' rows');
									} else if(sqlInfo[0].sql_typ == 'S') {
										executeDebug('selected ' + results.length + ' rows');
										result = results;
									}
									i++;
									c();
								}
							}); // end of query (excute sql)
						} // end of if-else
					}); // end of query (get program)
				},
				function(err){
					if(err){
						rollback(err);
						endConnection(err);
						pool.end(function(err){
							if(err) errorDebug(err);
							else connectionDebug('delete connection pool');
						});
						callback(err);
					} else {
						commit();
						endConnection();
						pool.end(function(err){
							if(err) errorDebug(err);
							else connectionDebug('delete connection pool');
						});
						callback(null, result);
					}
				}
			); // end of whilst
		});	// end of transaction
			
		function endConnection (err) {
			if(err) {
				errorDebug(err);
			}

			connection.release();
			connectionDebug('return database connection');
		} // end of endConnection
	
		function rollback(err){
			if(err) {
				errorDebug(err);
			}
			
			connection.rollback(function(){
				errorDebug('rollback');
			});
		} // end of rollback

		function commit(){
			connection.commit(function(err){
				if(err){
					rollback(err);
					return;
				}

				executeDebug('commit');
			});
		} // end of commit
	});	// end of get connection 
};
 

module.exports = requesthandler;
